from django.conf.urls import url

from .views import dashboard, register, edit, user_detail, user_list, user_follow
from django.contrib.auth import views

urlpatterns = [
    # url(r'^login/$', views.user_login, name='login')
    url(r'^$', dashboard, name='dashboard'),
    url(r'^register/$', register, name='register'),
    url(r'^edit/$', edit, name='edit'),

    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^logout-then-login/$', views.logout_then_login, name='logout-then-login'),

    url(r'^password-change/$', views.PasswordChangeView.as_view(), name='password_change'),
    url(r'^password-change/done$', views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    url(r'^password-reset/$', views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^password-reset/done/$', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$', views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'),
    url(r'^password-reset/complete/$', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    url(r'^users/$', user_list, name='user_list'),
    url(r'^users/follow/$', user_follow, name='user_follow'),  # must come before user_detail
    url(r'^users/(?P<username>[-\w]+)/$', user_detail, name='user_detail'),
]
